<?php

return array(
    'baseUrl' => 'http://skeleton.local',
    'redirectUri' => '/user/index',
    'db' => [
        'host' => 'localhost',
        'name' => 'skeleton',
        'user' => 'root',
        'pass' => 'rootpass'
    ]
);