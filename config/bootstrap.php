<?php

use Zend\Session\SessionManager;
use Zend\Session\ManagerInterface;
use Zend\Session\Config\SessionConfig;
use Monolog\ErrorHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface as Logger;
use Tamtamchik\SimpleFlash\Flash;
use Zend\Config\Config;
use Skeleton\Acl\Acl;

$containerBuilder = new \DI\ContainerBuilder;
/* @var \DI\Container $container */
$container = $containerBuilder
//    ->addDefinitions(require_once __DIR__ . '/config_web.php')
    ->build();

//@TODO setup caching
$container->set(\FastRoute\Dispatcher::class, function() {
    $routeList = require __DIR__.'/../config/routes.php';

    /** @var \FastRoute\Dispatcher $dispatcher */
    return FastRoute\simpleDispatcher(
        function (\FastRoute\RouteCollector $r) use ($routeList) {
            foreach ($routeList as $routeDef) {
                $r->addRoute($routeDef[0], $routeDef[1], $routeDef[2]);
            }
        }
    );
});

$container->set(Acl::class, function() {
    return new Acl(require __DIR__.'/../config/acl.php');
});

$container->set(Skeleton\Middleware\AuthMiddleware::class, function() use ($container) {
    $aclData = require __DIR__.'/../config/acl.php';

    return new \Skeleton\Middleware\AuthMiddleware(
        $container->get(ManagerInterface::class),
        $container->get(Flash::class),
        $aclData
    );
});

$container->set(Config::class, function() {
    $params = include(APP_PATH . "/../config/config.php");
    $config = new \Zend\Config\Config($params);
    $config = $config->merge(new \Zend\Config\Config(include(APP_PATH . "/../config/config-local.php")));

    return $config;
});

$container->set(\Twig_Environment::class, function() use ($container) {
    $loader = new Twig_Loader_Filesystem(
        __DIR__ . '/../themes/default/'
    );

    $te = new Twig_Environment($loader, array(
        'debug' => true,
    ));
    $te->addExtension(new Twig_Extension_Debug());
    $te->addExtension(new Twig_Extensions_Extension_I18n());

    if ($container->get(ManagerInterface::class)->getStorage()->user) {
        $te->addGlobal('userId', $container->get(ManagerInterface::class)->getStorage()->user->getUserId());
    }

    return $te;
});

$container->set(Flash::class, function () use ($container) {
    //session needs to be started for flash
    $container->get(ManagerInterface::class);
    $flash = new Flash();

    return $flash;
});

$container->set(ManagerInterface::class, function() {
    $sessionConfig = new SessionConfig();
    $sessionConfig->setOptions([
        'remember_me_seconds' => 2592000, //2592000, // 30 * 24 * 60 * 60 = 30 days
        'use_cookies'         => true,
        'cookie_httponly'     => true,
        'name'                => 'wellsite',
    ]);

    $session = new SessionManager($sessionConfig);
    $session->start();

    return $session;
});

$container->set(Logger::class, function() {
    $logger = new \Monolog\Logger('skeletonlog');

    $date = new \DateTime('now', new \DateTimeZone('Europe/Belgrade'));
    $logDir = APP_PATH . '/../data/logs/' . $date->format('Y') . '-' . $date->format('m');
    $logFile = $logDir . '/' . gethostname() . '-' . $date->format('d') . '.log';
    $debugLog = APP_PATH . '/../data/logs/debug.log';
    // create dir or file if needed
    if (!is_dir($logDir)) {
        mkdir($logDir);
    }
    if (!file_exists($logFile)) {
        touch($logFile);
    }

    $logger->pushHandler(
        new StreamHandler($logFile)
    );
    $logger->pushHandler(
        new StreamHandler(
            $debugLog,
            \Monolog\Logger::DEBUG
        )
    );

    $logger->pushHandler(new BrowserConsoleHandler());

    ErrorHandler::register($logger);

    return $logger;
});

$container->set(FluentPDO::class, function() use ($container) {
    $config = $container->get(Config::class);
    $dsn = "mysql:host={$config->db->host};dbname={$config->db->name}";
    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );
    $pdo = new PDO($dsn, $config->db->user, $config->db->pass, $options);

    return new FluentPDO($pdo);
});

$container->set('remoteDb', function() use ($container) {
    $config = $container->get(Config::class);
    $dsn = "mysql:host={$config->remoteDb->host};dbname={$config->remoteDb->name}";
    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );
    $pdo = new PDO($dsn, $config->remoteDb->user, $config->remoteDb->pass, $options);

    return new FluentPDO($pdo);
});

$container->set(DateTime::class, function() use ($container) {
    return new \DateTime('now', new DateTimeZone('Europe/Belgrade'));
});



return $container;