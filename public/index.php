<?php
define('APP_PATH', __DIR__ . '/../Skeleton');

error_reporting(E_ALL);
ini_set('display_errors', 1);

include("../vendor/autoload.php");

/* @var \DI\Container $container */
$container = require __DIR__ . '/../config/bootstrap.php';

/** @var \FastRoute\Dispatcher $dispatcher */
$dispatcher = $container->get(\FastRoute\Dispatcher::class);

$route = $dispatcher->dispatch(
    $_SERVER['REQUEST_METHOD'],
    $_SERVER['REQUEST_URI']
);

$request = \GuzzleHttp\Psr7\ServerRequest::fromGlobals();
$response = new \GuzzleHttp\Psr7\Response();
$twig = $container->get(Twig_Environment::class);

// @TODO move all this to skeletor handle method...
switch ($route[0]) {
    case \FastRoute\Dispatcher::NOT_FOUND:
        $response->getBody()->write($twig->render('error404.twig', [
            'error' => 'Resource does not exist.'
        ]));
        break;
    case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $response->getBody()->write($twig->render('error405.twig', [
            'error' => 'Method is not allowed.'
        ]));
        break;
    case \FastRoute\Dispatcher::FOUND:
        $controller = $route[1];
        $parameters = $route[2];

        // @TODO why...
        foreach ($parameters as $name => $value) {
            $request = $request->withAttribute($name, $value);
        }

        try {
            $next = $container->get($controller);

            //@TODO configure proper middleware, and THEN refactor ACL :P
            $response = $container->call(\Skeleton\Middleware\AuthMiddleware::class, [
                $request, $response, $next
            ]);
        } catch (\Exception $e) {
            $response = \Skeleton\App\WebSkeletor::handleErrors(
                $e, $container->get(Psr\Log\LoggerInterface::class), $twig
            );
        }
//        $container->get(Psr\Log\LoggerInterface::class)->notice('running app controller : ' . $controller);

        break;
}

\Skeleton\App\WebSkeletor::respond($response);