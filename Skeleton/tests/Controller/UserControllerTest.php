<?php

namespace Test\Skeleton\Controller;

class UserControllerTest extends \PHPUnit_Framework_TestCase
{

    public function testIndexMethodShouldReturnResponseOk()
    {
        $_SESSION = [];
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->getMockForAbstractClass();
        $twigMock = $this->getMockBuilder(\Twig_Environment::class)
            ->setMethods(['render'])
            ->getMockForAbstractClass();
        $sessionMock = $this->getMockBuilder(\Zend\Session\ManagerInterface::class)
            ->setMethods(['getStorage'])
            ->getMockForAbstractClass();
        $configMock = $this->getMockBuilder(\Zend\Config\Config::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $requestMock = $this->getMockBuilder(\GuzzleHttp\Psr7\ServerRequest::class)
            ->disableOriginalConstructor()
            ->setMethods(['getAttribute'])
            ->getMockForAbstractClass();
        $userRepoMock = $this->getMockBuilder(\Wellsite\Model\Repository\UserRepository::class)
            ->setMethods(['fetchAllUsers'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $validatorMock = $this->getMockBuilder(\Wellsite\Validator\User::class)
            ->setMethods(['fetchUserByEmail'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $requestMock->expects(static::once())
            ->method('getAttribute')
            ->willReturn('index');
        $response = new \GuzzleHttp\Psr7\Response();

        $controller = new \Skeleton\Controller\UserController($twigMock, $sessionMock, $flashMock, $configMock, $userRepoMock, $validatorMock);
        $response = $controller($requestMock, $response);
        $this->assertSame(200, $response->getStatusCode());

//        $this->assertSame('/mainboard/index', $response->getHeader('Location')[0]);
    }
}