<?php

namespace Test\Recipe\Validator;

class UserTest extends \PHPUnit_Framework_TestCase
{

    public function testValidatorShouldReturnTrue()
    {
        $_SESSION = [];
        $userData = [
            'firstName' => 'test',
            'lastName' => 'test',
            'initial' => 'test',
            'level' => 1,
            'email' => 'test@example.com',
            'password' => 'testtest',
            'password2' => 'testtest'
        ];
        $userRepoMock = $this->getMockBuilder(\Wellsite\Model\Repository\UserRepository::class)
            ->setMethods(['fetchUserByEmail'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $userRepoMock->expects(static::once())
            ->method('fetchUserByEmail')
            ->willReturn(false);
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->getMockForAbstractClass();

        $validator = new \Skeleton\Validator\User($flashMock, $userRepoMock);
        $this->assertTrue($validator->isValid($userData));
    }

    public function testValidatorShouldReturnFalseAndSetErrors()
    {
        $_SESSION = [
            'flash_messages' => []
        ];
        $userData = [
            'firstName' => 't',
            'lastName' => 't',
            'initial' => 'test',
            'level' => 1,
            'email' => 'test',
            'password' => 'test',
            'password2' => 'testtest'
        ];
        $errors = [
            'error' => [
                "Email you entered is not valid.",
                "Email you entered already exists in system.",
                "Passwords you entered do not match.",
                "First name must be at least 3 characters long.",
                "Last name must be at least 3 characters long.",
                "Password must be at least 6 characters long."
            ]
        ];

        $userRepoMock = $this->getMockBuilder(\Wellsite\Model\Repository\UserRepository::class)
            ->setMethods(['fetchUserByEmail'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $userRepoMock->expects(static::once())
            ->method('fetchUserByEmail')
            ->willReturn(new \Wellsite\Model\Entity\User());
        $flashMock = $this->getMockBuilder(\Tamtamchik\SimpleFlash\Flash::class)
            ->getMockForAbstractClass();

        $validator = new \Wellsite\Validator\User($flashMock, $userRepoMock);
        $this->assertFalse($validator->isValid($userData));
        $this->assertSame($errors, $_SESSION['flash_messages']);
    }
}