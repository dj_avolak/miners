<?php

namespace Skeleton\Middleware;

use Zend\Session\ManagerInterface;
use Skeleton\Model\Entity\User;
use Tamtamchik\SimpleFlash\Flash;

class AuthMiddleware
{
    /**
     * @var ManagerInterface
     */
    private $sessionManager;

    private $aclData;

    /**
     * @var Flash
     */
    private $flash;

    const GUEST_LEVEL = 0;

    /**
     * AuthMiddleware constructor.
     *
     * @param ManagerInterface $sessionManager session manager
     */
    public function __construct(ManagerInterface $sessionManager, Flash $flash, $aclData)
    {
        $this->sessionManager = $sessionManager;
        $this->aclData = $aclData;
        $this->flash = $flash;
    }

    /**
     * Checks if user can access resource.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request  request
     * @param \Psr\Http\Message\ResponseInterface      $response response
     * @param null|callable                            $next     next
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke(
        \Psr\Http\Message\ServerRequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response,
        callable $next = null
    ) {
        $path = $request->getUri()->getPath();

        if (!$this->loggedIn() && !in_array($path, $this->aclData[self::GUEST_LEVEL])) {
            $this->flash->error('You need to be logged in to access page : ' . $request->getUri()->getPath());
            $this->sessionManager->getStorage()->redirectUri = $request->getUri()->getPath();
            return $response->withStatus(302)->withHeader('Location', '/login/index');
        }

        if ($this->checkAcl($request->getUri()->getPath())) {
            return $next($request, $response);
        } else {
            $this->flash->error('You do not have permissions to access page : ' . $request->getUri()->getPath());
            return $response->withStatus(302)->withHeader('Location', '/user/index');
        }
    }

    protected function loggedIn()
    {
        if ($this->sessionManager->getStorage()->user instanceof User) {
            return true;
        }

        return false;
    }

    protected function checkAcl($requestedPath)
    {
        $level = self::GUEST_LEVEL;
        if ($this->sessionManager->getStorage()->user instanceof User) {
            $level = $this->sessionManager->getStorage()->user->getRole();
        }

        foreach ($this->aclData[$level] as $path) {
            $wildcard = strstr($path, '*', true);
            if ($wildcard && strpos($requestedPath, $wildcard) !== false) {
                return true;
            }
            if ($requestedPath === $path) {
                return true;
            }
        }

        return false;
    }
}