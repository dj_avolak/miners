<?php

namespace Skeleton\Controller;

use Tamtamchik\SimpleFlash\Flash;
use Zend\Session\ManagerInterface;
use Zend\Config\Config;

class IndexController extends Controller
{

    /**
     * Controller constructor.
     *
     * @param \Twig_Environment $twig
     * @param ManagerInterface $sessionManager
     * @param Flash $flash
     * @param Config $config
     */
    public function __construct(
        \Twig_Environment $twig,
        ManagerInterface $sessionManager,
        Flash $flash,
        Config $config
    ) {
        parent::__construct($twig, $sessionManager, $flash, $config);
    }

    /**
     *
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index()
    {
        return $this->render('index', ['message' => 'Home page']);
    }
}