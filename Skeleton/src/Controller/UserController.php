<?php

namespace Skeleton\Controller;

use Zend\Session\ManagerInterface;
use Skeleton\Model\Repository\UserRepository;
use Skeleton\Model\Entity\User;
use Skeleton\Validator\User as UserValidator;
use Tamtamchik\SimpleFlash\Flash;
use Zend\Config\Config;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var UserValidator
     */
    private $validator;

    /**
     * Controller constructor.
     *
     * @param \Twig_Environment $twig
     * @param ManagerInterface $sessionManager
     * @param Config $config
     * @param Flash $flash
     * @param UserRepository $userRepo
     * @param UserValidator $validator
     */
    public function __construct(
        \Twig_Environment $twig,
        ManagerInterface $sessionManager,
        Flash $flash,
        Config $config,
        UserRepository $userRepo,
        UserValidator $validator
    ) {
        parent::__construct($twig, $sessionManager, $flash, $config);
        $this->userRepo = $userRepo;
        $this->validator = $validator;
    }

    public function index()
    {
        $users = $this->userRepo->fetchAllUsers();
        $data['case'] = 'userindex';

        return $this->render('index', [
            'data' => $data,
            'users' => $users
        ]);
    }

    public function form()
    {
        $data = $this->request->getParsedBody();
        if (isset($data['save']) && $this->validator->isValid($data)) {
            if (strlen($data['password']) > 0) {
                $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
            }

            try {
                if ((int) $data['userId'] === 0) {
                    $this->create($data);
                } else {
                    $this->update($data);
                }
            } catch (\Exception $e) {
                $this->flash->error('Could not save user.');

                return $this->render('form', [
                    'data' => $data,
                ]);
            }
            $this->flash->success('User saved successfully.');

            return $this->redirect('/user/index');
        }

        //display user data when updating
        $userId = $this->request->getAttribute('userId', null);
        if ((int) $userId > 0) {
            $user = $this->userRepo->fetchUserById($userId);
            $data = $user->getArrayCopy();
        }
        //set human readable level options
        $data['levels'] = User::hrLevels();

        //render form
        return $this->render('form', [
            'data' => $data,
        ]);
    }



    protected function create($data)
    {
        $user = new \Skeleton\Model\Entity\User();
        $user->exchangeArray($data);
        $this->userRepo->createUser($user);
    }

    protected function update($data)
    {
        $user = $this->userRepo->fetchUserById($data['userId']);
        $user->exchangeArray($data);
        $this->userRepo->updateUser($user);
    }

    public function delete()
    {
        $data = $this->request->getParsedBody();
        if (!isset($data['userId'])) {
            throw new \InvalidArgumentException('No data given to process');
        }

        try {
            $user = $this->userRepo->fetchUserById($data['userId']);
            $this->userRepo->deleteUser($user);
            $this->flash->success('User successfully deleted.');
        } catch (\Exception $e) {
            //@TODO log and display error page, do this in index.php
            var_dump($e->getMessage());
            die();
        }

        return $this->redirect('/user/index');
    }
}