<?php

namespace Skeleton\Controller;

use Psr\Http\Message\ResponseInterface;
use Tamtamchik\SimpleFlash\Flash;
use Zend\Session\ManagerInterface;
use Skeleton\Model\Repository\UserRepository;
use Skeleton\Model\Entity\User;
use Zend\Config\Config;

class LoginController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * Controller constructor.
     *
     * @param \Twig_Environment $twig
     * @param ManagerInterface $sessionManager
     * @param UserRepository $userRepo
     * @param Flash $flash
     * @param Config $config
     */
    public function __construct(
        \Twig_Environment $twig,
        ManagerInterface $sessionManager,
        UserRepository $userRepo,
        Flash $flash,
        Config $config
    ) {
        $this->userRepo = $userRepo;
        parent::__construct($twig, $sessionManager, $flash, $config);
    }

    /**
     * Display login form
     */
    public function index()
    {
//        if ($this->sessionManager->getStorage()->user instanceof User) {
//            return $this->redirect('/user/index');
//        }

        return $this->render('form');
    }

    public function logout()
    {
        $this->sessionManager->getStorage()->user = null;
        $this->flash->success('You have successfully logged out your account.');

        return $this->redirect('/');
    }

    /**
     * Tries to login user.
     *
     * @return ResponseInterface
     */
    public function login()
    {
        $user = $this->userRepo->fetchUser(['email' => $this->request->getParsedBody()['email']]);
        if (isset($user[0]) && password_verify($this->request->getParsedBody()['password'], $user[0]->getPassword())) {
            $this->sessionManager->getStorage()->user = $user[0];

            if ($this->sessionManager->getStorage()->redirectUri) {
                return $this->redirect($this->sessionManager->getStorage()->redirectUri);
            }
            return $this->redirect('/user/index');
        }
        $this->flash->error('Login failed.');

        return $this->redirect('/login/index');
    }
}
