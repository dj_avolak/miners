<?php
declare(strict_types = 1);
namespace Skeleton\Acl;


class Acl
{
    private $aclData;

    public function __construct($aclData)
    {
        $this->aclData = $aclData;
    }

    public function canRoleAccessPath($role, $requestedPath)
    {


        foreach ($this->aclData[$level] as $path) {
            $wildcard = strstr($path, '*', true);
            if ($wildcard && strpos($requestedPath, $wildcard) !== false) {
                return true;
            }
            if ($requestedPath === $path) {
                return true;
            }
        }

        return false;
    }
}