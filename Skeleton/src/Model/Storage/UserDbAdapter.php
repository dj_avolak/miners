<?php
declare(strict_types = 1);
namespace Skeleton\Model\Storage;

use Skeleton\Model\Entity\User;

class UserDbAdapter implements UserStorageInterface
{
    /**
     * @var \PDO
     */
    private $driver;

    /**
     * @var \FluentPDO
     */
    private $builder;

    private $tableName = 'user';

    public function __construct(\FluentPDO $fpdo)
    {
        $this->driver = $fpdo->getPdo();
        $this->builder = $fpdo;
    }

    public function fetchAllUsers($params = array(), $limit = null)
    {
        $sql = "SELECT * FROM `{$this->tableName}` ";
        $i = 0;
        foreach ($params as $name => $value) {
            if ($i === 0) {
                $sql .= " WHERE `{$name}` = '{$value}' ";
            } else {
                $sql .= " AND `{$name}` = '{$value}' ";
            }
            $i++;
        }
        if (isset($limit)) {
            $sql .= " LIMIT " . (int) $limit;
        }
        $stmt = $this->driver->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_CLASS, User::class);
    }

    /**
     * @param string $userId
     * @return User
     */
    public function fetchSingleUser($userId)
    {
        $sql = "SELECT * FROM `{$this->tableName}` WHERE `userId` = :userId";
        $stmt = $this->driver->prepare($sql);
        $stmt->execute([':userId' => (int) $userId]);

        return $stmt->fetchObject(User::class);
    }

    /**
     * Tries to create user and throws exception if failed.
     *
     * @param User $user
     * @throws \Exception
     *
     * @return void
     */
    public function createUser(User $user)
    {
        $names = array();
        $values = array();
        foreach ($user->getArrayCopy() as $attribute => $value) {
            $names[] = $attribute;
            $values[] = $value;
        }
        $names = implode(',', $names);
        $values = implode("','", $values);
        $sql = "INSERT INTO `{$this->tableName}` ({$names}) VALUES('{$values}')";

        $result = $this->driver->prepare($sql)->execute();
        if (!$result) {
            throw new \Exception($this->driver->errorInfo()[0]);
        }
    }

    public function updateUser(User $user)
    {
        $values = '';
        //@TODO check if password is updated
        foreach ($user->getArrayCopy() as $attribute => $value) {
            $values .= $attribute . "='" . $value . "',";
        }
        $sql = "UPDATE `{$this->tableName}` SET " . rtrim($values, ",") . " WHERE userId = " . $user->getUserId();

        $result = $this->driver->prepare($sql)->execute();
        if (!$result) {
            throw new \Exception($this->driver->errorInfo());
        }
    }

    public function deleteUser(User $user)
    {
        $sql = "DELETE FROM `{$this->tableName}` WHERE `userId` = {$user->getUserId()}";

        return $this->driver->prepare($sql)->execute();
    }

}