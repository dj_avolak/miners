<?php

namespace Skeleton\Model\Storage;

use Skeleton\Model\Entity\User;
use Zend\Stdlib\ArrayObject;

interface UserStorageInterface
{
    /**
     * Fetches a list of ArticleEntity models.
     *
     * @param array $params
     *
     * @return ArrayObject
     */
    public function fetchAllUsers($params = array());

    /**
     * Fetches a single ArticleEntity model.
     *
     * @param string $articleUuid
     *
     * @return User
     */
    public function fetchSingleUser($articleUuid);

    /**
     * Creates article model.
     *
     * @param User $article
     *
     * @return bool
     */
    public function createUser(User $article);

    /**
     * Updates article model.
     *
     * @param User $article
     *
     * @return bool
     */
    public function updateUser(User $article);

    /**
     * Deletes a single article entity.
     *
     * @param User $article
     *
     * @return bool
     */
    public function deleteUser(User $article);
}