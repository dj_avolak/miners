<?php
declare(strict_types = 1);
namespace Skeleton\Model\Repository;

use Skeleton\Model\Entity\User;
use Skeleton\Model\Storage\UserDbAdapter;
use Zend\Stdlib\ArrayObject;

/**
 * Class UserRepository.
 *
 *
 * @package Wellsite\Model\Repository
 */
class UserRepository
{
    /**
     * @var UserDbAdapter
     */
    private $userStorage;

    /**
     * @var \DateTime
     */
    private $dateTime;

    /**
     * ArticleRepository constructor.
     *
     * @param UserDbAdapter $userStorage
     * @param \DateTime $dateTime
     */
    public function __construct(UserDbAdapter $userStorage, \DateTime $dateTime)
    {
        $this->userStorage = $userStorage;
        $this->dateTime = $dateTime;
    }

    /**
     * Fetches a list of UserEntity models.
     *
     * @param array $params
     *
     * @return ArrayObject
     */
    public function fetchAllUsers($params = array())
    {
        return $this->userStorage->fetchAllUsers($params);
    }

    /**
     * Fetches a single User by params
     *
     * @param array $params
     *
     * @return User
     */
    public function fetchUser($params = array())
    {
        $limit = 1;
        return $this->userStorage->fetchAllUsers($params, $limit);
    }

    /**
     * Fetches a single User by params
     *
     * @param string $email
     *
     * @return User
     */
    public function fetchUserByEmail($email)
    {
        return $this->fetchUser(['email' => $email]);
    }

    /**
     * Fetches a single User model by id.
     *
     * @param int $userId
     *
     * @return User
     */
    public function fetchUserById($userId)
    {
        return $this->userStorage->fetchSingleUser($userId);
    }

    /**
     * Creates user model.
     *
     * @param User $user
     *
     * @return bool
     */
    public function createUser(User $user)
    {
        return $this->userStorage->createUser($user);
    }

    /**
     * Updates user model.
     *
     * @param User $user
     *
     * @return bool
     */
    public function updateUser(User $user)
    {
        return $this->userStorage->updateUser($user);
    }

    /**
     * Deletes a single user entity.
     *
     * @param User $user
     *
     * @return bool
     */
    public function deleteUser(User $user)
    {
        return $this->userStorage->deleteUser($user);
    }
}
