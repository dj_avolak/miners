<?php

namespace Skeleton\App;

use \Psr\Log\LoggerInterface as Logger;
use \Psr\Http\Message\ResponseInterface;
use \GuzzleHttp\Psr7\Response as Response;

class WebSkeletor
{
    /**
     * Handle errors and return response object.
     *
     * @param \Exception $exception
     * @param Logger $logger
     * @param \Twig_Environment $twig
     *
     * @return ResponseInterface
     */
    public static function handleErrors(\Exception $exception, Logger $logger, \Twig_Environment $twig)
    {
        $msg = $exception->getMessage();
        $response = new Response();

        switch (get_class($exception)) {
            case \InvalidArgumentException::class:
                $response->getBody()->write($twig->render('error400.twig', [
                    'error' => $msg
                ]));

                break;
            case \Exception::class:
//            break;

            default:
                $response->getBody()->write($twig->render('error50x.twig', [
                    'error' => $msg,
                    'trace' => $exception->getTrace()
                ]));

                break;
        }

        $logger->error($msg);
        // @TODO send email notification

        return $response;
    }


    public static function respond(ResponseInterface $response) {
        // Send response
        if (!headers_sent()) {
            // Status
            header(sprintf(
                'HTTP/%s %s %s',
                $response->getProtocolVersion(),
                $response->getStatusCode(),
                $response->getReasonPhrase()
            ));

            // Headers
            foreach ($response->getHeaders() as $name => $values) {
                foreach ($values as $value) {
                    header(sprintf('%s: %s', $name, $value), false);
                }
            }
        }

//        $body = $response->getBody();
//        $body->rewind();
//
//    echo $body->getContents();


        // Body
        if (!in_array($response->getStatusCode(), [204, 205, 304])) {
            $body = $response->getBody();
            if ($body->isSeekable()) {
                $body->rewind();
            }
            $chunkSize = 4096;
            $contentLength  = $response->getHeaderLine('Content-Length');
            if (!$contentLength) {
                $contentLength = $body->getSize();
            }

            if (isset($contentLength)) {
                $amountToRead = $contentLength;
                while ($amountToRead > 0 && !$body->eof()) {
                    $data = $body->read(min($chunkSize, $amountToRead));
                    echo $data;

                    $amountToRead -= strlen($data);

                    if (connection_status() != CONNECTION_NORMAL) {
                        break;
                    }
                }
            } else {
                while (!$body->eof()) {
                    echo $body->read($chunkSize);
                    if (connection_status() != CONNECTION_NORMAL) {
                        break;
                    }
                }
            }
        }
    }
}