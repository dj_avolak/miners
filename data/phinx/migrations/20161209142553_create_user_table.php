<?php

use Phinx\Migration\AbstractMigration;

class CreateUserTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = "CREATE TABLE `user` (
  `userId` INT NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(45),
  `lastName` VARCHAR(45),
  `email` VARCHAR(128),
  `password` VARCHAR(128),
  `createdAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `status` INT,
  `role` INT,
  PRIMARY KEY (`userId`));
  ADD UNIQUE INDEX `email_UNIQUE` (`email` ASC);
";
        $this->query($sql);

        // insert default user
        $pass = '$2y$10$GGArVO/7.xPDg6D5Kl6GHeELUg2Dnod68ynkFaZ7R2Vfx/K1oZ96O'; // testtest
        $sql = "INSERT INTO `user` (
`firstName`, `lastName`, `email`, `createdAt`, `status`, `role`, `password`
) VALUES (
'test', 'test', 'test@example.com', '0', '1', '1', '{$pass}');";
        $this->query($sql);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->query("DROP TABLE `user`");
    }
}